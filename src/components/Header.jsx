import React, {useContext} from 'react'
import '../Assets/Styles.css'
import {Link} from 'react-router-dom'
import {usercontext} from '../Firebase_API/Authcontext'
import {signOut} from 'firebase/auth'
import {auth} from '../Firebase_API/Firebase'
import {toast} from 'react-toastify'
import CreateProduct from './Products/CreateProduct'
import ProductData from './Products/ProductData'


export default function Header() {
    let user = useContext(usercontext)
  let data=JSON.stringify(user)
    let logout = async() => {
        await signOut(auth)
        window
            .sessionStorage
            .removeItem("Token");
        toast.success('Logout Successful')
        window
            .location
            .assign('/signup')
    }
  
    function LoggedIn() {
        return ( <> <div>
            <nav className="navbar">
                <div className="logo">E-Site</div>
                <ul className="nav-links">
                    <input type="checkbox" id="checkbox_toggle"/>
                    <label className="hamburger">&#9776;</label>
                    <div className="menu">
                        <li>
                            <span
                                style={{
                                fontSize: 18,
                                textTransform: "uppercase"
                            }}>{user.displayName}{user.mobilenum} ( Admin )</span>
                        </li>
                        <li>
                            <Link to='/home'>Home</Link>

                        </li>
                        <li>
                            <a onClick={logout}>Logout</a>
                        </li>
                    </div>
                </ul>
            </nav>
            {/* <CreateProduct/> */}
            {/* <ProductData/> */}

        </div> </>
      )
    }

    function LoggedOut(){
        return ( <> <div>
            <nav className="navbar">
                <div className="logo">E-Site</div > <ul className="nav-links">
            <input type="checkbox" id="checkbox_toggle"/>
            <label  className="hamburger">&#9776;</label>
            <div className="menu">

                <li>
                    <Link to='/login'>Login</Link>
                </li>
                <li className="services">
                    <Link to='/signup'>SignUp</Link>
                </li>
                <li>
                    <Link to='/adminlogin'>Admin Login</Link>
                </li>

            </div>
        </ul> </nav>
        </div > </>)
    }
    return <> {
        user
            ? <LoggedIn/>
            : <LoggedOut/>
    } </>
}
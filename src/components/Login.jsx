import React, {useState} from 'react'
import {toast} from 'react-toastify'
import {auth} from '../Firebase_API/Firebase'
import { useNavigate } from 'react-router-dom'
import {signInWithEmailAndPassword,signInWithPhoneNumber} from 'firebase/auth'


export default function Login() {
    let navigate=useNavigate()

    let [email,
        setEmail] = useState();
    let [password,
        setPassword] = useState()

    let handleLogin = async(e) => {
        e.preventDefault();
        console.log(email,password)
        let userdata=await signInWithEmailAndPassword(auth,email,password)
        if(userdata.user.emailVerified === true)
        {
            toast.success("User Login Successfully")
            navigate('/')
        }
        else if(userdata.user.providerData === password){
            toast.error('Please Give Correct Password & Email')

        }
        else{
            toast.error('Please Check Your Email is Verified ?')
        }
    }

    return (
        <div className='container mt-4 w-50 '>
            <div className='card m-auto'>
                <div className='card-body'>
                    <form onSubmit={handleLogin}>
                        <h1>Login</h1>
                        <input
                            type='text'
                            placeholder='Enter Mobile Number'
                            className='form-control mt-2'
                            onChange={(e) => {
                            setEmail(e.target.value)
                        }}></input>
                        <input
                            type='text'
                            placeholder='Enter Password'
                            className='form-control mt-2'
                            onChange={(e) => {
                            setPassword(e.target.value)
                        }}></input>
                        <button className='btn btn-md btn-success mt-2'>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

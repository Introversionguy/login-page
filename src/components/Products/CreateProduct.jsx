import React from 'react'
import {useState} from 'react'
import {database } from '../../Firebase_API/Firebase'
import { Database,set,ref,onValue } from 'firebase/database'

export default function CreateProduct() {
    let [Name,
        setName] = useState()
    let [Price,
        setPrice] = useState()
    let [Url,
        setUrl] = useState()
    let [Detail,
        setDetail] = useState()
    let [Rating,
        setRating] = useState()
    let data={pName:Name,pPrice:Price}
    let handleProduct = (e) => {
        e.preventDefault();
        try {
            let send = async() => {
                await set(ref(database, "Product_Data")+data.Name, {Name, Price, Url, Detail, Rating});
            };
            send() 

        } catch (error) {}
        console.log(Name, Price, Url, Detail, Rating)

    }
    return ( <> <div className='card bg-light mt-4 w-50'>
        <div className='card-body w-75 m-auto'>
            <h1>Product</h1>
            <div className='card-body mt-2'>
                <form onSubmit={handleProduct}>
                    <input
                        placeholder='Name'
                        className='form-control mb-2'
                        required
                        onChange={(e) => {
                        setName(e.target.value)
                    }}></input>
                    <input
                        placeholder='Price'
                        className='form-control mb-2'
                        required
                        onChange={(e) => {
                        setPrice(e.target.value)
                    }}></input>
                    <input
                        placeholder='Product URL'
                        className='form-control mb-2'
                        required
                        onChange={(e) => {
                        setUrl(e.target.value)
                    }}></input>
                    <input
                        placeholder='Product Details'
                        className='form-control mb-2'
                        required
                        onChange={(e) => {
                        setDetail(e.target.value)
                    }}></input>
                    <input
                        placeholder='Rating'
                        className='form-control mb-2'
                        required
                        onChange={(e) => {
                        setRating(e.target.value)
                    }}></input>
                    <button className='btn btn-success' >Add Product</button>
                </form>
            </div>
        </div>
    </div> </>
  )
}
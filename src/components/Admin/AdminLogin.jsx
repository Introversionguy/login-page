import React, {useState} from 'react'
import {toast} from 'react-toastify'
import {auth} from '../../Firebase_API/Firebase'
import { useNavigate } from 'react-router-dom'
import {signInWithEmailAndPassword} from 'firebase/auth'
import '../../Assets/Admin.css'

export default function AdminLogin() {
    let navigate=useNavigate()

    let [email,
        setEmail] = useState();
    let [password,
        setPassword] = useState()

    let handleLogin = async(e) => {
        e.preventDefault();
        console.log(email,password)
        let userdata=await signInWithEmailAndPassword(auth,email,password)
        if(userdata.user.emailVerified === true)
        {
            toast.success("User Login Successfully")
            navigate('/create')
        }
        else if(userdata.user.providerData === password){
            toast.error('Please Give Correct Password & Email')

        }
        else{
            toast.error('Please Check Your Email is Verified')
        }
    }

  return (
    <>
   
    <div className="container1">
	<div className="screen">
		<div className="screen__content">
			<form className="login" onSubmit={handleLogin}>
                <h1>Admin</h1>
				<div className="login__field">
					<i class="fa fa-user login__icon" aria-hidden="true"></i>
					<input type="text" className="login__input" placeholder="User name / Email"        onChange={(e) => {
                            setEmail(e.target.value)
                        }}/>
				</div>
				<div className="login__field">
					<i class="fa fa-lock login__icon" aria-hidden="true"></i>
					<input type="password" className="login__input" placeholder="Password"        onChange={(e) => {
                            setPassword(e.target.value)
                        }}/>
				</div>
				<button className="button login__submit">
					<span className="button__text">Log In Now</span>
					<i class="fa fa-chevron-right button__icon" aria-hidden="true"></i>
				</button>				
			</form>
			<div className="social-login">
				<h3>log in via</h3>
				<div className="social-icons">
					<a href="#" className="social-login__icon fab"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
					<a href="#" className="social-login__icon "><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></a>
					<a href="#" className="social-login__icon fab"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		<div className="screen__background">
			<span className="screen__background__shape screen__background__shape4"></span>
			<span className="screen__background__shape screen__background__shape3"></span>		
			<span className="screen__background__shape screen__background__shape2"></span>
			<span className="screen__background__shape screen__background__shape1"></span>
		</div>		
	</div>
</div>
    </>
  )
}

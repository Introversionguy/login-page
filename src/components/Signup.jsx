import React, {useState} from 'react'
import firebase, {auth} from '../Firebase_API/Firebase'
import {createUserWithEmailAndPassword, sendEmailVerification, updateProfile} from 'firebase/auth'
import {toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useNavigate } from 'react-router-dom'
import {async} from '@firebase/util'

export default function Signup() {
    let navigate=useNavigate()
    let [name,
        setUser] = useState()
        let [mobile,
            setmobile] = useState()
    let [email,
        setEmail] = useState()
    let [password,
        setPassword] = useState()
    let [conPass,
        setConPass] = useState()

    let handleform = async(e) => {
        e.preventDefault();
        console.log(name, email, password, conPass)
        try {
            if (password !== conPass) {
                toast.error('Password is not Match')
            } else {
                let userdata = await createUserWithEmailAndPassword(auth, email, password,mobile)
                toast.success('User Created Successfully')
                console.log(userdata)
                let user=userdata.user;
                sendEmailVerification(user);
                toast.success(`Please Confirm Your Email ${email}`)
                updateProfile(user ,{ displayName:name, mobilenum:mobile})
                navigate('/login')
            }
        } catch (error) {
            console.log(error)
        }

    }
    return ( <>
     <div className='container w-50'>
        <div className='card '>
            <div className='card-body'>
                <form onSubmit={handleform}>
                    <h1>SignUp</h1>
                    <input
                        type='text'
                        placeholder='Enter Username'
                        className='form-control mt-2'
                        onChange={(e) => {
                        setUser(e.target.value)
                    }}></input>
                    <input
                        type='text'
                        placeholder='Enter Email'
                        className='form-control mt-2'
                        onChange={(e) => {
                        setEmail(e.target.value)
                    }}></input>
                        <input
                        type='text'
                        placeholder='Enter mobile number'
                        className='form-control mt-2'
                        onChange={(e) => {
                        setmobile(e.target.value)
                    }}></input>
                    <input
                        type='password'
                        placeholder='Enter Password'
                        className='form-control mt-2'
                        onChange={(e) => {
                        setPassword(e.target.value)
                    }}></input>
                    <input
                        type='password'
                        placeholder='Confirm Password'
                        className='form-control mt-2'
                        onChange={(e) => {
                        setConPass(e.target.value)
                    }}></input>

                    <button className='btn btn-md btn-success mt-2'>Submit</button>
                </form>
            </div>
        </div>
    

    </div>
     </>
  )
}
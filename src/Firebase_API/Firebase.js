// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth"
import {getDatabase} from 'firebase/database'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD38AC6fyRGEaEKDjuBdcnemRiTF-jiPIk",
  authDomain: "student-form-data-live.firebaseapp.com",
  projectId: "student-form-data-live",
  storageBucket: "student-form-data-live.appspot.com",
  messagingSenderId: "335711304429",
  appId: "1:335711304429:web:de08503c61ba9404bb6ae4",
  measurementId: "G-LV9Y4Y81B4"
};

// const firebaseConfig = {
//   apiKey: "AIzaSyBfvsmxl4t-mT4BKzHUBoWRVMwLSN2qwPA",
//   authDomain: "product-data-8bcbc.firebaseapp.com",
//   projectId: "product-data-8bcbc",
//   storageBucket: "product-data-8bcbc.appspot.com",
//   messagingSenderId: "864671376670",
//   appId: "1:864671376670:web:803bf0b585b501573fd50f",
//   measurementId: "G-QV3ELKM7SW"
// };

// Initialize Firebase
const firebase = initializeApp(firebaseConfig)
const analytics = getAnalytics(firebase)

export let auth=getAuth(firebase)
export let database=getDatabase(firebase)

export default firebase;
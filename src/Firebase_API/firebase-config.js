import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyD38AC6fyRGEaEKDjuBdcnemRiTF-jiPIk",
    authDomain: "student-form-data-live.firebaseapp.com",
    projectId: "student-form-data-live",
    storageBucket: "student-form-data-live.appspot.com",
    messagingSenderId: "335711304429",
    appId: "1:335711304429:web:de08503c61ba9404bb6ae4",
    measurementId: "G-LV9Y4Y81B4"
  };

  // const firebaseConfig = {
  //   apiKey: "AIzaSyBfvsmxl4t-mT4BKzHUBoWRVMwLSN2qwPA",
  //   authDomain: "product-data-8bcbc.firebaseapp.com",
  //   projectId: "product-data-8bcbc",
  //   storageBucket: "product-data-8bcbc.appspot.com",
  //   messagingSenderId: "864671376670",
  //   appId: "1:864671376670:web:803bf0b585b501573fd50f",
  //   measurementId: "G-QV3ELKM7SW"
  // };
  

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
import { Container, Row, Col } from "react-bootstrap";
import { Routes, Route ,BrowserRouter} from "react-router-dom";
import Home from "./Phone_Auth/Home";
import Login from "./Phone_Auth/Login";
import Signup from "./Phone_Auth/Signup";
import PhoneSignUp from "./Phone_Auth/PhoneSignUp";
import ProtectedRoute from "./Phone_Auth/ProtectedRoute";
import { UserAuthContextProvider } from "./context/UserAuthContext";

function PhoneAuth() {
  return (
    <Container style={{ width: "400px" }}>
      <Row>
        <Col>
          <UserAuthContextProvider>
           <BrowserRouter>
           <Routes>
              <Route
                path="/home"
                element={
                  <ProtectedRoute>
                    <Home />
                  </ProtectedRoute>
                }
              />
              {/* <Route path="/" element={<Login />} /> */}
              {/* <Route path="/signup" element={<Signup />} /> */}
              {/* <Route path="/phonesignup" element={<PhoneSignUp />} /> */}
            </Routes>
           </BrowserRouter>
          </UserAuthContextProvider>
        </Col>
      </Row>
    </Container>
  );
}

export default PhoneAuth;

import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyD38AC6fyRGEaEKDjuBdcnemRiTF-jiPIk",
  authDomain: "student-form-data-live.firebaseapp.com",
  projectId: "student-form-data-live",
  storageBucket: "student-form-data-live.appspot.com",
  messagingSenderId: "335711304429",
  appId: "1:335711304429:web:de08503c61ba9404bb6ae4",
  measurementId: "G-LV9Y4Y81B4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export default app;
